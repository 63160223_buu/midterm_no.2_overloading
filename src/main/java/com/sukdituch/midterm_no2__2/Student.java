/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.midterm_no2__2;

/**
 *
 * @author focus
 */
public class Student {

    private String oprt;//สร้างตัวแปร
    private double n1;//สร้างตัวแปร
    private int n2;//สร้างตัวแปร
    public double pi = 22.0 / 7;//สร้างตัวแปร และกำหนดค่า

    public Student(String oprt, double n1, int n2) { // Constructor & Overloading
        this.oprt = oprt;
        this.n1 = n1;
        this.n2 = n2;
    }

    public Student(String oprt, double n1 ) {// Constructor & Overloading
        this.oprt = oprt;
        this.n1 = n1;
    }

    public double getN1() {// ส่ง n1 ออกไปใช้ในสมการต่อ
        return n1;
    }

    public double getN2() {// นำค่า n2 ที่เก็บไว้ออกไปใช้ในสมการต่อ
        return n2;
    }

    public double calArea() {// สร้างสมการหาค่า
        switch (oprt) {// สลับ case ที่ได้รับมา เพื่อหาค่าตามที่ต้องการ
            case "+":
                return n1 + n2;

            case "-":
                return n1 - n2;

            case "*":
                return n1 * n2;

            case "/":
                return n1 / n2;

        }
        return n1;// รีเทรินค่า n1 เมื่อไม่มีการกำหนดค่า n2
    }
}
