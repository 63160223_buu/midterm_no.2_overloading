/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.midterm_no2__2;

/**
 *
 * @author focus
 */
public class TestStudent {

    public static void main(String[] args) {
        Student studentno1 = new Student("+", 11, 12); // สร้างobject แบบเงื่อนไขแรก
        System.out.println("Answer is : "+studentno1.calArea());
        System.out.println(" ");

        Student studentno2 = new Student("-", 12, 11);
        System.out.println("Answer is : "+studentno2.calArea());
        System.out.println(" ");

        Student studentno3 = new Student("*", 12, 11);
          System.out.println("Answer is : "+studentno3.calArea());
        System.out.println(" ");

        Student studentno4 = new Student("/", 100, 10);
        System.out.println("Answer is : "+studentno4.calArea());
        System.out.println(" ");

        Student studentno5 = new Student("+", 123);// สร้างobject แบบเงื่อนไขที่สอง
        System.out.println("Answer is : "+studentno5.calArea());
        System.out.println(" ");
    }
}
